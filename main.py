import math
import random
from typing import Generator, Tuple

import pyglet
from pyglet import shapes, text
from typing_extensions import Self

Color = Tuple[int, int, int]


class Point:
    distance: float
    color: Color
    size: float

    def __init__(self: Self, x: float, y: float, name: str = "") -> None:
        self.x = x
        self.y = y
        self.name = name

    def compare(self: Self, other: "Point"):
        if self.distance < other.distance:
            return -1

        if self.distance > other.distance:
            return 1

        return 0


def generate_points(count: int, max: int) -> list[Point]:
    """Generates a list of points

    Args:
        count (int): Size of list
        max (int): Max x or y position of point

    Returns:
        list[Point]
    """
    return [
        Point(x=(random.random() * max), y=(random.random() * max))
        for _ in range(count)
    ]


class PathFinding:
    def __init__(self: Self, shuttle: "Shuttle") -> None:
        self._shuttle = shuttle

    def find_path_manhattan(self: Self) -> Tuple[float, float, bool, bool]:
        position = self._shuttle.position
        target_position = self._shuttle.target_position
        result_x, result_y = (
            target_position.x - position.x,
            target_position.y - position.y,
        )

        x_positive = result_x >= 0
        y_positive = result_y >= 0

        distance_x = round(abs(result_x), 2)
        distance_y = round(abs(result_y), 2)

        return distance_x, distance_y, x_positive, y_positive

    def find_path(self: Self) -> Tuple[float, bool, bool]:
        position = self._shuttle.position
        target_position = self._shuttle.target_position

        a, b = target_position.x - position.x, target_position.y - position.y
        c = math.sqrt(a**2 + b**2)

        x_positive = a >= 0
        y_positive = b >= 0

        return c, x_positive, y_positive


def drange(start: float, stop: float, step: float):
    """Range, but with `float` step

    Args:
        start (float)
        stop (float)
        step (float)

    Yields:
        float: Current value
    """
    r = start
    while r < stop:
        yield r
        r += step


class Shuttle:
    available: bool = True
    target_position: Point
    has_package: bool = False

    def __init__(
        self: Self, position: Point, id: int, color: Color, in_blocks: bool = True
    ) -> None:
        self.position = position
        self.id = id
        self.move_in_blocks = in_blocks
        self.color = color

    def dispatch(self: Self, destination: Point) -> Generator:
        print(f"Shuttle {self.id} moving to {destination.x:.2f}, {destination.y:.2f}")
        self.available = False
        self.target_position = destination
        return self.move()

    def move(self: Self):
        s = self
        path_finding = PathFinding(s)
        step_size = 1

        if s.move_in_blocks:
            path = path_finding.find_path_manhattan()

            for _ in drange(0, path[0], step_size):
                if path[2]:
                    s.position.x += step_size
                else:
                    s.position.x -= step_size
                yield True

            for _ in drange(0, path[1], step_size):
                if path[3]:
                    s.position.y += step_size
                else:
                    s.position.y -= step_size
                yield True

            if s.target_position.x != 50 and s.target_position.y != 50:
                s.has_package = True

            print(f"Shuttle {s.id} finished operation. Waiting for instructions.")
        else:
            path = path_finding.find_path()
            slope = abs(
                (s.target_position.y - s.position.x)
                / (s.target_position.x - s.position.x)
            )

            while not (
                abs(s.position.x - s.target_position.x) < step_size
                and abs(s.position.y - s.target_position.y) < step_size
            ):
                if path[1]:
                    s.position.x += 0.2
                else:
                    s.position.x -= 0.2

                if path[2]:
                    s.position.y += 0.2 * slope
                else:
                    s.position.y -= 0.2 * slope

                if path[1]:
                    if s.position.x > s.target_position.x:
                        break
                else:
                    if s.position.x < s.target_position.x:
                        break

                if path[2]:
                    if s.position.y > s.target_position.y:
                        break
                else:
                    if s.position.y < s.target_position.y:
                        break

                yield True

            if s.target_position.x != 50 and s.target_position.y != 50:
                s.has_package = True

            print(f"Shuttle {s.id} finished operation. Waiting for instructions.")

        s.available = True


SIZE = 500
COLOR = (50, 225, 30)

# Antialiasing for shapes
config = pyglet.gl.Config()
config.sample_buffers = 1  # type: ignore
config.samples = 4  # type: ignore

window = pyglet.window.Window(SIZE, SIZE, config=config)


class Draw:
    def __init__(self, points, shuttles) -> None:
        self.points = points
        self.shuttles = shuttles
        self.move_gens = []
        self.draw()

    def draw(self):
        self.background_batch = pyglet.graphics.Batch()
        self.point_graphics = []
        self.label_graphics = []
        self.background = shapes.Rectangle(
            0, 0, SIZE, SIZE, (255, 255, 255), batch=self.background_batch
        )

        for p in self.points:
            self.point_graphics.append(
                shapes.Circle(
                    p.x,
                    p.y,
                    getattr(p, "size", 2),
                    color=getattr(p, "color", COLOR),
                    batch=self.background_batch,
                )
            )
            if getattr(p, "name", None):
                self.label_graphics.append(
                    text.Label(
                        p.name,
                        font_name="Times New Roman",
                        font_size=10,
                        x=p.x + 5,
                        y=p.y,
                        color=(100, 100, 100, 100),
                        batch=self.background_batch,
                    ),
                )

        self.shuttles_batch = pyglet.graphics.Batch()
        self.shuttle_graphics = []
        for s in self.shuttles:
            self.shuttle_graphics.append(
                shapes.Circle(
                    s.position.x,
                    s.position.y,
                    3,
                    color=s.color,
                    batch=self.shuttles_batch,
                )
            )

    def on_draw(self):
        window.clear()
        self.background_batch.draw()
        self.shuttles_batch.draw()

    def update(self, dt):
        for move_gen_i, move_gen in enumerate(self.move_gens):
            shuttle_i, gen = move_gen

            # Move one pixel
            if next(gen, False):
                self.shuttle_graphics[shuttle_i].x = self.shuttles[shuttle_i].position.x
                self.shuttle_graphics[shuttle_i].y = self.shuttles[shuttle_i].position.y
            else:
                self.move_gens.pop(move_gen_i)
                self.shuttles[shuttle_i].has_package = False

        # There are more moves to make before doing another dispatch
        if len(self.move_gens) > 0:
            return

        for shuttle_i, shuttle in enumerate(self.shuttles):
            if shuttle.available:
                if shuttle.has_package:
                    moves = shuttle.dispatch(self.points[50])
                else:
                    # Dispatch to random package, but not center
                    index = random.randint(1, N) - 1
                    if index != 50:
                        moves = shuttle.dispatch(self.points[index])
                    else:
                        moves = shuttle.dispatch(self.points[49])
                self.move_gens.append([shuttle_i, moves])
                continue


if __name__ == "__main__":
    SIZE = 500
    N = 100

    points = generate_points(N, SIZE)

    # Dispatch-point
    points[50].x = SIZE / 2
    points[50].y = SIZE / 2
    points[50].name = "Drop off"
    points[50].color = (245, 40, 145)
    points[50].size = 4

    point = points[8]
    shuttles = [
        Shuttle(point, 1, (52, 103, 235), False),
        Shuttle(
            point,
            2,
            (235, 201, 52),
        ),
    ]

    # shuttles[0].dispatch(points[17])
    # shuttles[1].dispatch(points[13])
    canvas = Draw(points, shuttles)

    @window.event
    def on_draw():
        canvas.on_draw()

    pyglet.clock.schedule_interval(func=canvas.update, interval=1 / 120)
    pyglet.app.run()
